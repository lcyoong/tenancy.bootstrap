<?php

return [
    'tenant_path' => env('TENANCYBOOTSTRAP_PATH', '/t'),
    'super_roles' => ['admin'],
    'database_prefix' => 'tenant-',
];
