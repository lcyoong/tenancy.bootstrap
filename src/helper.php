<?php

if (! function_exists('tenant_url')) {
    function tenant_url($path)
    {
        return url(config('tenancybootstrap.tenant_path').'/'.$path);
    }
}


if (! function_exists('switch_tenant')) {
    function switch_tenant($tenant_id)
    {
        $environment = app(Hyn\Tenancy\Environment::class);

        $website = app(Hyn\Tenancy\Contracts\Repositories\WebsiteRepository::class)->findById($tenant_id);

        $environment->tenant($website);
    }
}
