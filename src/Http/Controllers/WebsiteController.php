<?php

namespace App;

use App\Http\Controllers\Controller;
use Lcyoong\TenancyBootstrap\Traits\WebsiteManagement;

class WebsiteController extends Controller
{
    use WebsiteManagement;
}
