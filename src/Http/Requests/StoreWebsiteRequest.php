<?php

namespace Lcyoong\TenancyBootstrap\Http\Middleware\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWebsiteRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'uuid' => 'required|unique:websites,uuid',
          'user_email' => 'required|email|unique:users,email',
        ];
    }
}
