<?php

namespace Lcyoong\TenancyBootstrap\Http\Middleware;

use Closure;

class TenancyRightfulUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user) {
            // Set active tenant into session
            $tenant_id = session('active_tenant_id');

            // Reject access: if not the landlord or doesn't have a house or if it's not his house
            // if (!$user->hasRole(config('tenancybootstrap.super_roles')) && (empty($user->website) || $user->website->id != $tenant_id)) {
            if (!$user->hasRole(config('tenancybootstrap.super_roles')) && !$user->websites->contains($tenant_id)) {
                return abort(403);
            }

            // Connect to the tenancy database
            switch_tenant($tenant_id);
        }

        // Pass the request
        return $next($request);
    }
}
