<?php

namespace Lcyoong\TenancyBootstrap\Http\Middleware;

use Closure;

class RemoveTenantSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->forget(['active_tenant', 'active_tenant_id']);

        // Pass the request
        return $next($request);
    }
}
