<?php

namespace Lcyoong\TenancyBootstrap\Http;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Hyn\Tenancy\Models\Website;

class WebsiteUser extends Model
{
    protected $fillables = ['wu_website', 'wu_user', 'wu_role'];

    /**
     * Relationship to user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'wu_user');
    }

    /**
     * Relationship to website
     *
     * @return void
     */
    public function website()
    {
        return $this->belongsTo(Website::class, 'wu_website');
    }
}
