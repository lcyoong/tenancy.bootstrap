<?php

namespace Lcyoong\TenancyBootstrap\Traits;

use Illuminate\Http\Request;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Lcyoong\TenancyBootstrap\Http\Middleware\Requests\StoreWebsiteRequest;
use App\User;
use Artisan;
use Lcyoong\TenancyBootstrap\Traits\WebsiteDatabase;
use Lcyoong\TenancyBootstrap\Traits\WebsiteSession;
use Lcyoong\TenancyBootstrap\Http\WebsiteUser;

trait WebsiteManagement
{
    use WebsiteDatabase, WebsiteSession;
    /**
     * List websites
     *
     * @return void
     */
    public function index()
    {
        $list = Website::get();

        return view($this->viewPath('website.index'), compact('list'));
    }

    /**
     * Website create page
     *
     * @return void
     */
    public function create()
    {
        return view($this->viewPath('website.new'));
    }

    /**
     * Website edit page
     *
     * @return void
     */
    public function edit($id)
    {
        $website = Website::findOrFail($id);

        return view($this->viewPath('website.edit'), compact('website'));
    }

    /**
     * Save new website
     *
     * @param Request $request
     * @return void
     */
    public function store(StoreWebsiteRequest $request)
    {
        // Create user
        $user = User::firstOrCreate(
            ['email' => $request->user_email],
            ['name' => $request->user_email, 'password' => bcrypt('11111111')]
        );

        // Create website/tenancy
        if ($user) {
            $website = new Website;
            $website->uuid = config('tenancybootstrap.database_prefix').$request->uuid;
            // $website->user = $user->id;

            app(WebsiteRepository::class)->create($website);
            
            // Attach user to website
            $user->websites()->attach($website->id, ['wu_role' => 1]);
        }

        if ($website && $user) {
            return $this->postStore($website, $user);
        }
    }

    /**
     * Assign user to website
     *
     * @param Request $request
     * @return void
     */
    public function assignUserToWebsite(Request $request)
    {
        $website = Website::findOrFail($request->website_id);

        $user = User::findOrFail($request->user_id);

        $result = $user->websites()->attach($website->id, ['wu_role' => $request->role_id ?? 2]);

        return $this->postAssignUserToWebsite($result);
    }

    public function update(Request $request)
    {
        $website = Website::findOrFail($request->id);

        $result = $website->update($request->input());

        return $this->postUpdate($result);
    }

    /**
     * Switch tenant - direct to tenant
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function switchTenant(Request $request, $id)
    {
        $this->setSession($id);

        return redirect(tenant_url('/'));
    }


    /**
     * View path for website module
     *
     * @param [type] $string
     * @return void
     */
    private function viewPath($string)
    {
        return 'tenancybootstrap::'.$string;
    }

    /**
     * After website update action
     *
     * @param [type] $result
     * @return void
     */
    protected function postUpdate($result)
    {
        return null;
    }

    /**
     * After user assignment to website action
     *
     * @param [type] $result
     * @return void
     */
    protected function postAssignUserToWebsite($result)
    {
        return null;
    }

    /**
     * After website store action
     *
     * @param [type] $website
     * @param [type] $user
     * @return void
     */
    protected function postStore($website, $user)
    {
        if ($website && $user) {
            return response([
                'data' => ['tenant' => $website->id, 'user'=> $user->id],
                'message' => 'Successful'
            ]);
        }
    }
}
