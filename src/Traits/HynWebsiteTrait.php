<?php

namespace Lcyoong\TenancyBootstrap\Traits;

trait HynWebsiteTrait
{
    public function scopeActive($query)
    {
        return $query->where('status', '=', 'active');
    }
}
