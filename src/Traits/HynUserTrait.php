<?php

namespace Lcyoong\TenancyBootstrap\Traits;

use App\Website;

trait HynUserTrait
{
    public function websites()
    {
        return $this->belongsToMany(Website::class, 'website_users', 'wu_user', 'wu_website');
    }
}
