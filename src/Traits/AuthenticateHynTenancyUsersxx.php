<?php

namespace Lcyoong\TenancyBootstrap\Traits;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Lcyoong\TenancyBootstrap\Traits\WebsiteSession;

trait AuthenticateHynTenancyUsersxx
{
    use WebsiteSession;
    /**
     * Post authentication - redirect tenant to tenancy
     *
     * @param Request $request
     * @param [type] $user
     * @return void
     */
    protected function authenticated(Request $request, $user)
    {
        $hostname  = app(\Hyn\Tenancy\Environment::class)->hostname();

        // Tenant without a house - log out
        if (!$user->hasRole(config('tenancybootstrap.super_roles')) && (!$user->website || $user->website->status != 'active')) {
            $this->logout($request);

            throw ValidationException::withMessages([
                $this->username() => [trans('auth.notenancy')],
            ]);
        }

        // Log in via tenant house
        if ($hostname) {

            // Admin - direct to tenant house
            if ($user->hasRole(config('tenancybootstrap.super_roles'))) {
                $this->setSession($hostname->website_id);

                return redirect(tenant_url('/'));
            }
            // Tenant accesses the wrong house - log out
            elseif ($hostname->website_id != $user->website->id) {
                $this->logout($request);
                
                throw ValidationException::withMessages([
                    $this->username() => [trans('auth.wrongtenancy')],
                ]);
            }
        }

        // Rightful tenant - direct to tenant house
        if ($user->website) {
            $this->setSession($hostname->website_id);

            return redirect(tenant_url('/'));
        }
    }
}
