<?php

namespace Lcyoong\TenancyBootstrap\Traits;

use Illuminate\Http\Request;
use Artisan;

trait WebsiteDatabase
{
    /**
     * Refresh database (reset and re-run all migrations)
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function refreshDatabase(Request $request, $id)
    {
        Artisan::call("tenancy:migrate:refresh --website_id={$id}");
    }

    /**
     * Migrate database
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function migrateDatabase(Request $request, $id)
    {
        Artisan::call("tenancy:migrate --website_id={$id}");
    }
}
