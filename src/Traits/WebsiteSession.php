<?php

namespace Lcyoong\TenancyBootstrap\Traits;

use Illuminate\Http\Request;
use Hyn\Tenancy\Models\Website;

trait WebsiteSession
{
    /**
     * Set the website session
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function setSession($id)
    {
        $website = Website::findOrFail($id);

        session(['active_tenant_id' => $id]);

        session(['active_tenant' => $website]);
    }
}
