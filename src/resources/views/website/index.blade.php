@extends('layouts.app')

@section('content')
<div class="container">
    <table>
        @foreach($list as $item)
        <tr>
            <td>{{ $item->uuid }}</td>
            <td>{{ $item->status }}</td>
            <td><a href="{{ url('admin/tenants/switch/'.$item->id) }}">Switch</a></td>
            <td><a href="{{ url('admin/tenants/refresh/'.$item->id) }}">Refresh</a></td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
