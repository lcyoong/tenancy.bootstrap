@extends('layouts.app')

@section('content')
<div class="container">
    <ajax-form method="post" action="{{ url('admin/tenants/new') }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="uuid" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="user_email" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </ajax-form>
</div>
@endsection
