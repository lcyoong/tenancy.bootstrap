<?php

namespace Lcyoong\TenancyBootstrap;

use Illuminate\Support\ServiceProvider;

class TenancyBootstrapServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'tenancybootstrap');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->publishes([
            __DIR__.'/config/tenancybootstrap.php' => config_path('tenancybootstrap.php'),
            __DIR__.'/resources/views/website' => resource_path('views/vendor/tenancybootstrap/website'),
            __DIR__.'/Http/Controllers' => app_path('Http/Controllers')
        ]);

        $file = __DIR__.'/helper.php';

        if (file_exists($file)) {
            require_once($file);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/tenancybootstrap.php',
            'tenancybootstrap'
        );
    }
}
