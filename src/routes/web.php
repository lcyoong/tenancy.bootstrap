<?php

Route::group(['middleware' => ['web', 'auth', 'role:admin', 'remove_tenant_session'], 'namespace' => 'App', 'prefix' => '/admin'], function () {
    Route::group(['prefix' => '/tenants'], function () {
        Route::get('/', 'WebsiteController@index');
        Route::get('/new', 'WebsiteController@create');
        Route::get('/{tenant}/edit', 'WebsiteController@edit');
        Route::get('/{tenant}/switch', 'WebsiteController@switchTenant');
        Route::post('/new', 'WebsiteController@store');
        Route::post('/user/assign', 'WebsiteController@assignUserToWebsite');
        Route::post('/{tenant}/refresh', 'WebsiteController@refreshDatabase');
        Route::post('/{tenant}/migrate', 'WebsiteController@migrateDatabase');
        Route::put('/{tenant}', 'WebsiteController@update');
    });
});
